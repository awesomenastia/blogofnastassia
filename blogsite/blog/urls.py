from django.urls import path, re_path
from . import views
from .feeds import LatestPostsFeed

app_name = 'blog'
urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('instagram/', views.insta_view, name='insta_posts'),
    path(r'tag/<slug:tag_slug>/', views.post_list, name='post_list_by_tag'),
    # path('', views.PostListView.as_view(), name='post_list'),
    path(r'<int:year>/<int:month>/<int:day>/<slug:post>/', views.post_detail, name='post_detail'),
    path(r'<int:post_id>/share/', views.post_share, name='post_share'),
    path('feed/', LatestPostsFeed(), name='post_feed')
]
