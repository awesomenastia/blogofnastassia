Django==2.0
pytz
django-taggit==0.22.2
Markdown==2.6.11
django-instagram==0.2.0a1